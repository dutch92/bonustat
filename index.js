const db_path = require('./settings_loc.js')
const consts = require('./consts.js');
const got = require('got');
const cheerio = require('cheerio');
const co = require('co');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(db_path);

const fs = require('fs');
const util = require('util');
const log_error = fs.createWriteStream(__dirname + '/error.log', { flags : 'a' });
const log_stdout = process.stdout;

console.error = function(...args) {
  log_error.write(util.format(args.join(' ')) + '\n');
  log_stdout.write(util.format(args.join(' ')) + '\n');
};

co(function*() {
  yield createTabels();

  const cleaner = new Cleaner();
  const goods = {};
  const users = {};
  let lots = yield getOriginLots();

  yield updateLotsHistory(lots, goods);

  let lastTime = Date.now() / 1000 - 5;
  let execTime = 0;

  while(true) {
    if(cleaner.canStartClean()) yield cleaner.clean();
    try {
      if(execTime / 1000 > 20 ) {
        execTime = 0;

        clearUsers(users);
        clearGoods(goods);

        lots = yield getOriginLots();

        yield updateLotsHistory(lots, goods);
      }
    } catch(err) {
      console.error(err, 'WHEN EXECTIME > 20 sec', Date.now());
    }

    const start = Date.now();
    try {
      const form = {};
      lots.active.forEach((lot, i) => {
        form[`lotlist[${i}][saleid]`] = lot.id;
        form[`lotlist[${i}][rendertype]`] = 'main';
      });

      const result = (yield got.post('https://bonusmall.ru/ajax.php?action=salestate&changeid=' + lastTime, { body: form, timeout: 5000 })).body;

      if(/403 Forbidden/.test(result)) throw new Error('ERROR FORBIDEN');

      const stats = JSON.parse(result);

      for(let key in stats.sales) {
        if(stats.sales[key].events) {
          const events = stats.sales[key].events;

          for (let i = 0; i < events.length; i++) {
            try {
              const e = events[i];
              switch(e.changetype) {
                case 1: {
                  let user = users[e.nick];
                  if(!user) {
                    const userInDB = yield get(`SELECT id, nick FROM users WHERE nick="${e.nick}"`);

                    if(userInDB) user = { id: userInDB.id, time: getNow() };
                    else {
                      user = {
                        id: yield insertRow(`INSERT INTO users (nick, createdAt) VALUES ("${e.nick}", ${getNow()})`),
                        time: getNow()
                      }
                    } 

                    users[e.nick] = user;
                  }
                  yield run("INSERT INTO bids VALUES (?, ?, ?, ?)", key, e.bidtm + (60 * 60 * 5), user.id, e.price);

                  break;
                }
                case 2: {
                  const completedLot = lots.active.find(l => l.id == key);

                  if(completedLot) {
                    const userIdQuery = `SELECT user_id FROM bids WHERE lot_id=${completedLot.id} ORDER BY t DESC LIMIT 1`;
                    const winner = yield get(`SELECT * FROM users WHERE id=(${userIdQuery})`);

                    console.log(`${getTime()}: Lot #${key}: ${completedLot.desc} has been completed. Winner: ${(winner ? winner.nick : '__NOUSER__')}`);
                  }

                  break;
                }
                default: {
                  console.log('Another changetype: ' + e.changetype, 'Result is: ' + result);
                  break;
                }
              }
            } catch(err) {
              console.error(err, 'ERROR IN EVENTS');
            }
          }
        }
      }

      lastTime = stats.changeid;
    } catch(err) {
      console.error(err, 'MAIN CYCLE ERROR', Date.now());
    }
    yield cb => setTimeout(cb, 2000);
    const end = Date.now();
    execTime += end - start;
  }

}).catch(err => console.error('ERROR', err, Date.now()));

function getLotsAsArray(fn, type) {
  const lotsArray = [];

  fn(`#lot_block div[name="lot${type}"]`).each((i, el) => {
    lotsArray.push({
      id: fn(el).attr('id').match(/item(\d+)/)[1],
      desc: fn(el).find('.desc_text').text(),
      url: fn(el).find('.postid').attr('href').match(/product\/(\d+)/)[1]
    });
  });

  return lotsArray;
}

function createTabels() {
  return co.wrap(function*() {
    yield run("CREATE TABLE IF NOT EXISTS 'lots_history' ('lot_id' INTEGER NOT NULL, 'good_id' INTEGER, 'status' INTEGER, 'complete_date' DATETIME, 'winner_id' INTEGER)");
    yield run("CREATE UNIQUE INDEX IF NOT EXISTS unique_lot on lots_history (lot_id)");
    yield run("CREATE TABLE IF NOT EXISTS 'goods' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'desc' TEXT, 'url' INTEGER, 'image' TEXT)");
    yield run("CREATE UNIQUE INDEX IF NOT EXISTS unique_good on goods (desc, url)");
    yield run("CREATE TABLE IF NOT EXISTS bids (lot_id INTEGER NOT NULL, t DATETIME, user_id INTEGER, p REAL)");
    yield run("CREATE UNIQUE INDEX IF NOT EXISTS unique_bid on bids (lot_id, p)");
    yield run("CREATE TABLE IF NOT EXISTS 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 'nick' TEXT, 'createdAt' DATETIME NOT NULL)");
    yield run("CREATE UNIQUE INDEX IF NOT EXISTS unique_user on users (nick)");
  })();
}

function run(...args) {
  return new Promise((res, rej) => {
    db.run(...args, (e, r) => !!e ? rej(e) : res(r));
  });
}

function insertRow(...args) {
  return new Promise((res, rej) => {
    db.run(...args, function(e, r) {
      if(!!e) {
        console.log('INSERT USER ERROR: ', e);
        return rej(e);
      } else {
        return res(this.lastID);
      }
    });
  });
}

function get(...args) {
  return new Promise((res, rej) => {
    db.get(...args, (e, r) => !!e ? rej(e) : res(r));
  });
}

function all(...args) {
  return new Promise((res, rej) => {
    db.all(...args, (e, r) => !!e ? rej(e) : res(r));
  });
}

function getOriginLots() {
  return co.wrap(function*() {
    const lots = {
      active: [],
      completed: [],
      future: []
    };

    const res1 = got('http://bonusmall.ru/auction/1', { timeout: 5000 });
    const res2 = got('http://bonusmall.ru/auction/2', { timeout: 5000 });

    const result = yield Promise.all([res1, res2]);

    result.forEach(res => {
      const body = res.body;

      if(/403 Forbidden/.test(body)) throw new Error('Forbidden error');

      const $ = cheerio.load(body);
      const activeLots = getLotsAsArray($, 'active');
      const completedLots = getLotsAsArray($, 'completed');
      const futureLots = getLotsAsArray($, 'future');

      lots.active = lots.active.concat(activeLots);
      lots.completed = lots.completed.concat(completedLots);
      lots.future = lots.future.concat(futureLots);
    });

    return lots;
  })();
}

function updateLotsHistory(lots, goods) {
  return co.wrap(function*() {
    const dbActive = yield all(`SELECT * FROM lots_history WHERE status=${consts.STATUS_ACTIVE}`);
    const dbFuture = yield all(`SELECT * FROM lots_history WHERE status=${consts.STATUS_FUTURE}`);
    const dbCompleted = yield all(`SELECT * FROM lots_history WHERE status=${consts.STATUS_COMPLETED}`);

    for(let i = 0; i < lots.future.length; i++) {
      const l = lots.future[i];
      const index = dbFuture.findIndex(item => item.lot_id == l.id);

      if(index > -1) dbFuture.splice(index, 1);
      else {
        const good = yield getGood(l, goods);
        console.log(`${getTime()}: trying to insert NEW lot ${l.id}: ${l.desc} set FUTURE`);
        try {
          yield run(`INSERT INTO lots_history (lot_id, good_id, status) VALUES (${l.id}, ${good.id}, ${consts.STATUS_FUTURE})`);
        } catch(err) {
          console.log(`${getTime()}:`, err);
        }
      }
    }

    for(let i = 0; i < lots.active.length; i++) {
      const l = lots.active[i];
      const index = dbActive.findIndex(item => item.lot_id == l.id);

      if(index > -1) dbActive.splice(index, 1);
      else {
        const futureIndex = dbFuture.findIndex(f => f.lot_id == l.id);

        if(futureIndex > -1) {
          console.log(`${getTime()}: trying to update lot ${l.id}: ${l.desc} set ACTIVE`);
          dbFuture.splice(futureIndex, 1);
          try {
            yield run(`UPDATE lots_history SET status=${consts.STATUS_ACTIVE} WHERE lot_id=${l.id}`);
          } catch(err) {
            console.log(`${getTime()}:`, err);
          }
        } else {
          const good = yield getGood(l, goods);
          console.log(`${getTime()}: trying to insert NEW lot ${l.id}: ${l.desc} set ACTIVE`);
          try {
            yield run(`INSERT INTO lots_history (lot_id, good_id, status) VALUES (${l.id}, ${good.id}, ${consts.STATUS_ACTIVE})`);
          } catch(err) {
            console.log(`${getTime()}:`, err);
          }
        }
      }
    }

    if(dbActive.length) {
      for(let i = 0; i < dbActive.length; i++) {
        const l = dbActive[i];
        const lastBid = yield get(`SELECT user_id FROM bids WHERE lot_id=${l.lot_id} ORDER BY t DESC LIMIT 1`);
        console.log(`${getTime()}: trying to update lot ${l.lot_id} set COMPLETED in dbActive`);
        try {
          yield run(`UPDATE lots_history SET status=${consts.STATUS_COMPLETED}, complete_date=${getNow()}, winner_id=${(lastBid ? lastBid.user_id : 0)} WHERE lot_id=${l.lot_id}`);
        } catch(err) {
          console.log(`${getTime()}:`, err);
        }
      }
    }
  })();
}

function getGood(lot, goods) {
  return co.wrap(function*() {
    let good = goods[lot.url];

    if(!good) {
      const goodInDB = yield get(`SELECT id, desc FROM goods WHERE url="${lot.url}"`);

      if(goodInDB) good = { id: goodInDB.id, time: getNow() };
      else {

        good = {
          id: yield insertRow(`INSERT INTO goods (desc, url) VALUES ("${lot.desc}", ${lot.url})`),
          time: getNow()
        };
      }

      goods[lot.url] = good;
    }

    return good;
  })();
}

function clearUsers(users) {
  if(Object.keys(users).length > 3000) {
    for(let key in users) {
      if(users[key].time + 3600 < getNow()) {
        delete users[key];
      }
    }
  }
}

function clearGoods(goods) {
  if(Object.keys(goods).length > 1000) {
    for(let key in goods) {
      if(goods[key].time + 3600 * 24 * 30 < getNow()) {
        delete goods[key];
      }
    }
  }
}

function getNow() {
  return Math.round(Date.now() / 1000);
}

function getTime() {
  const date = new Date();

  return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`; 
}

class Cleaner {
  constructor() {
    this.startTime = Date.now();
  }

  canStartClean() {
    return (Date.now() - this.startTime) > (1000 * 60 * 60 * 24);
  }

  async clean() {
    const res1 = got('http://bonusmall.ru/auction/1', { timeout: 5000 });
    const res2 = got('http://bonusmall.ru/auction/2', { timeout: 5000 });

    const result = await Promise.all([res1, res2]);
    let futureLots = [];

    result.forEach(res => {
      const body = res.body;

      if(/403 Forbidden/.test(body)) throw new Error('Forbidden error');

      const $ = cheerio.load(body);
      futureLots = futureLots.concat(getLotsAsArray($, 'future'));
    });

    const futureLotsIds = futureLots.map(l => l.id).join(',');

    try {
      await run(`DELETE FROM lots_history WHERE status=3 AND lot_id NOT IN (${futureLotsIds})`);
    } catch(err) {
      console.log(err);
    }

    this.startTime = Date.now();
    console.log('successfully cleaned!');
  }
}